---
title: "Můj Notre Dame"

description: "Stránka vygenerovaná pomocí statického generátoru Hugo"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

Literally. It uses the [`ananke` theme](https://github.com/theNewDynamic/gohugo-theme-ananke)
which supports content on your front page.
Edit `/content/en/_index.md` to change what appears here. Delete `/content/en/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
